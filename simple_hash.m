function hash = simple_hash(f1, f2, deltaT)
  hash = f2 + 2^8 * f1 + 2^16 * deltaT;
end