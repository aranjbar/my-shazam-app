function peaks = voiceprint (y, fs)
        % Preprocessing
        y = mean(y, 2);     % Average the two channels
        y = y - mean(y);    % Subtract the mean
        oldSampleRate = fs;
        newSampleRate = 8000;
        y = resample(y, newSampleRate, oldSampleRate); % Downsample

        % Spectrogram
        window = 64 * newSampleRate / 1000;
        noverlap = 32 * newSampleRate / 1000;
        nfft = window;
        [s, f, t] = spectrogram(y, window, noverlap, nfft, newSampleRate);
        % imagesc(abs(s))
        % set(gca, 'YScale', 'log') % Plot the log of the magnitude of the spectrogram
        
        % Spectrogram local peaks
        a = { ...
                [0, 1], ...   % to down
                [0, -1], ...  % to top
                [1, 0], ...   % to right
                [-1, 0], ...  % to left
                [1, 1], ...   % to right down
                [1, -1], ...  % to right up
                [-1, 1], ...  % to left down
                [-1, -1], ... % to left up
        };
        p = ones(size(s));
        sAbs = abs(s);
        for i = 1 : 8
                cs = circshift(s, a{i});
                p = p & ((sAbs - abs(cs)) > 0);
        end
        % imagesc(p)
        % colormap(1 - gray)

        % Thresholding
        % Average thresholding
        constellaionsPerSecond = 30;
        totalConstellation = round(t(end) * constellaionsPerSecond);
        constellaionsSorted = sort(sAbs(p), 'descend');
        threshold = constellaionsSorted(totalConstellation);
        p1 = p & sAbs >= threshold;
        
        % imagesc(P1)
        % colormap(1 - gray);

        % Adaptive thresholding
        chunks = floor(linspace(1, numel(t), ceil(t(end))));
        p2 = logical([]);
        chunks(end) = chunks(end) + 1;
        for i = 1 : numel(chunks) - 1
            interval = false(size(sAbs));
            interval(:, chunks(i) : chunks(i + 1) - 1) = 1;
            constellaionsSorted = sort(sAbs(p & interval), 'descend');
            threshold = constellaionsSorted(constellaionsPerSecond);
            chunk = reshape(sAbs(interval), size(sAbs, 1), []);
            p2 = cat(2, p2, chunk >= threshold);
        end
        p2 = p2 & p;
        
        % imagesc(P2)
        % colormap(1 - gray)

        peaks = p2;
end