clear; close all; clc

clips_path = uigetdir(pwd, 'Select clips folder');
clip_list = get_mp3_list(clips_path);
ccn = 0;
for i = 1 : length(clip_list)
  songid = clip_list{i};
  clip_path = fullfile(clips_path, songid);
  detected_songid = match_clip(clip_path);
  
  correct_songid = split(songid, '-');
  correct_songid = correct_songid{1};
  detected_songid = strip(split(detected_songid, '-'));
  detected_songid = split(detected_songid{2}, '.');
  detected_songid = detected_songid{1};
  if strcmp(correct_songid, detected_songid)
    ccn = ccn + 1;
  end
  disp(strcat('correct id: ', songid))
  disp(strcat('detected id: ', detected_songid))
  disp('_______________________________')
end
disp(ccn)
disp(length(clip_list))