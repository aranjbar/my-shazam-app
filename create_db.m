function create_db()
  table_dir = fileparts('Table/');
  hashtable_path = fullfile(table_dir, 'HASHTABLE.mat');
  songid_path = fullfile(table_dir, 'SONGID_DB.mat');
  if ~exist(table_dir, 'dir')
    disp('Creating "Table" directory...')
    [status, msg]  = mkdir('Table');
    if status ~= 1
      error(msg)
    end
  end  
  if ~exist(hashtable_path, 'file')
    disp('Creating "HASHTABLE.mat"...')
    hashtable_size = 2097152;
    hashtable = cell(hashtable_size, 2);
    save(hashtable_path, 'hashtable');
  end
  if ~exist(songid_path, 'file')
    disp('Creating "SONGID.mat"...')
    songid_db = cell(0);
    save(songid_path, 'songid_db');
  end
end