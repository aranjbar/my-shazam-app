clear; close all; clc

tic
create_db();
train_songs_dir = uigetdir(pwd, 'Select train folder');
mp3_list = get_mp3_list(train_songs_dir);
wb = waitbar(0,'1','Name','Train');
for i = 1 : length(mp3_list)
  songid = mp3_list{i};
  waitbar(i/length(mp3_list), wb, sprintf('%d / %d\n%s',i, length(mp3_list), songid))
  [y, fs] = audioread(fullfile(train_songs_dir, songid));
  peaks = voiceprint(y, fs);
  pairs = peak_to_pair(peaks);
  maxCollisions = add_to_db(pairs, songid);
end
close(wb);
toc
