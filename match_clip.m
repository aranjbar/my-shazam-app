function songid = match_clip(clip_path)
  [clip, fs] = audioread(clip_path);
  clip = awgn(clip, 15);
  peaks = voiceprint(clip, fs);
  pairs = peak_to_pair(peaks);

  hashtable_path = fullfile('Table/HASHTABLE.mat');
  songid_path = fullfile('Table/SONGID_DB.mat');
  hashtable = load(hashtable_path);
  songid_db = load(songid_path);
  hashtable = hashtable.hashtable;
  songid_db = songid_db.songid_db;
  num_songs = length(songid_db);
  matches = cell(num_songs, 1);

  for i = 1 : size(pairs, 1)
    t1 = pairs(i, 1); t2 = pairs(i, 2); f1 = pairs(i, 3); f2 = pairs(i, 4); 
    idx = simple_hash(f1, f2, t2 - t1);
    if ~isempty(hashtable{idx, 1})
      match_id = hashtable{idx, 1};
      match_t1 = hashtable{idx, 2};
      match_t0 = match_t1 - t1;
      for n = 1 : num_songs
        matches{n} = [matches{n}, match_t0(match_id == n)];
      end
    end
  end
  num_of_matches = zeros(size(matches));
  for i = 1 : num_songs
    num_of_matches(i) = length(matches{i});
  end

  [~, best_match_id] = max(num_of_matches);
  songid = songid_db{best_match_id};

  optional_plot = false;
  if optional_plot
    figure(3)
    clf
    y = zeros(length(matches),1);
    for k = 1:length(matches)
        subplot(length(matches),1,k)
        hist(matches{k},1000)
        y(k) = max(hist(matches{k},1000));
    end

    for k = 1:length(matches)
        subplot(length(matches),1,k)
        axis([-inf, inf, 0, max(y)])
    end

    subplot(length(matches),1,1)
    title('Histogram of offsets for each song')
  end
end