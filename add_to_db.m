function maxCollisions = add_to_db(pairs, songid)
  hashtable_path = fullfile('Table/HASHTABLE.mat');
  songid_path = fullfile('Table/SONGID_DB.mat');
  hashtable = load(hashtable_path);
  songid_db = load(songid_path);
  hashtable = hashtable.hashtable;
  songid_db = songid_db.songid_db;

  songid_num = find(contains(songid_db, songid));
  maxCollisions = 0;
  if isempty(songid_num)
    songid_num = length(songid_db) + 1;
    songid_db{songid_num} = songid;
      
    for i = 1 : length(pairs)
      t1 = pairs(i, 1); t2 = pairs(i, 2); f1 = pairs(i, 3); f2 = pairs(i, 4); 
      idx = simple_hash(f1, f2, t2 - t1);
      if isempty(hashtable{idx, 1})
        hashtable{idx, 1} = songid_num;
        hashtable{idx, 2} = t1;
      else
        hashtable{idx, 1} = [hashtable{idx, 1}, songid_num];
        hashtable{idx, 2} = [hashtable{idx, 2}, t1];
      end

      collisions = length(hashtable{idx, 1});
      if collisions > maxCollisions
          maxCollisions = collisions;
      end
    end

    save(hashtable_path, 'hashtable');
    save(songid_path, 'songid_db');
  end
end
